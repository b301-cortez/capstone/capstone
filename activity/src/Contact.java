public class Contact {
    // variables
    private String name;
    private String contactNumber;
    private String address;

    // default constructor
    public Contact(){}

    // parameterized constructor
    public Contact(String name, String contactNumber, String address){
        this.name = name;
        this.contactNumber = contactNumber;
        this.address = address;
    }

    // setters
    public void setName(String name){
        this.name = name;
    }

    public void setContactNumber(String contactNumber){
        this.contactNumber = contactNumber;
    }

    public void setAddress(String address){
        this.address = address;
    }

    // getters
    public String getName(){
        return name;
    }

    public String getContactNumber(){
        return contactNumber;
    }

    public String getAddress(){
        return address;
    }

}
