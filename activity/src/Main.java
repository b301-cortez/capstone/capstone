import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        // --------phonebook--------
        Phonebook phonebook = new Phonebook();

        // --------contact 1--------
        // contact 1 using default constructor
        Contact contact1 = new Contact();
        // used setter method to add value
        contact1.setName("John Doe");
        contact1.setContactNumber("+639152468596");
        contact1.setAddress("Quezon City");

        // --------contact 2--------
        // contact 2 using parameterized constructor
        Contact contact2 = new Contact("Jane Doe", "+639162148573", "Caloocan City");

        // adding contact1 and contact2 to phonebook using the setter method
        phonebook.addContact(contact1);
        phonebook.addContact(contact2);

        // control structure to know whether the phonebook is empty or not
        if (phonebook.size() == 0){
            System.out.println("Phonebook is empty");
        } else {
            // to read the elements in the array list and stores it to the variable "contacts" of data type "ArrayList<Contact>"
            ArrayList<Contact> contacts = phonebook.getContacts();

            // to iterate over the elements in the array list and stores it to the variable "contact" of data type "Contact"
            for (int i = 0; i < contacts.size(); i++){

                Contact contact = contacts.get(i);

                // name of contact
                System.out.println(contact.getName());

                System.out.println("------------------------");

                // contact number
                System.out.println(contact.getName() + " has the following registered number: ");
                System.out.println(contact.getContactNumber());

                // address of contact
                System.out.println(contact.getName() + " has the following registered address: ");
                System.out.println("my home is in " + contact.getAddress());

                System.out.println();
            }

        }
    }
}