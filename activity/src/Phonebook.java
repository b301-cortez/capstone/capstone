import java.util.ArrayList;

public class Phonebook {
    // composition variable
    private ArrayList<Contact> contacts;

    // default constructor
    public Phonebook(){
        this.contacts = new ArrayList<>();
    }

    // parameterized constructor
    public Phonebook(ArrayList<Contact> contacts){
        this.contacts = contacts;
    }

    // setters
    public void addContact(Contact contact){
        contacts.add(contact);
    }

    // getters
    public ArrayList<Contact> getContacts(){
        return contacts;
    }

    // to get the size of the array list
    public int size() {
        return contacts.size();
    }
}
